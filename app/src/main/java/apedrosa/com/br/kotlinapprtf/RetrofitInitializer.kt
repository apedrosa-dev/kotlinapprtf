package apedrosa.com.br.kotlinapprtf

import apedrosa.com.br.kotlinapprtf.service.NoteService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {

        private val retrofit = Retrofit.Builder()
                .baseUrl("http://192.168.0.23:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        // Single-Expression function
        // o retorno explícito se torna opcional
        // fun noteService(): NoteService
        fun noteService() = retrofit.create(NoteService::class.java)



}