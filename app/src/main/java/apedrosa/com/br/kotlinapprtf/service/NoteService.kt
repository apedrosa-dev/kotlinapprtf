package apedrosa.com.br.kotlinapprtf.service

import retrofit2.http.GET

interface NoteService {
    @GET("notes")
    fun list()
}