package apedrosa.com.br.kotlinapprtf.activities

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import apedrosa.com.br.kotlinapprtf.RetrofitInitializer

class NoteListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView()

        RetrofitInitializer()

        val recyclerView = note_list_recyclerview
        recyclerView.adapter = NoteLisAdapter(notes(), this)
        //restante da configuração do recyclerView
    }

    private fun notes(): List<Note> {
        //lista de exemplos
    }
}